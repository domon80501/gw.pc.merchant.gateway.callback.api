﻿using GW.PC.Core;
using Serilog;
using Serilog.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace GW.PC.Merchant.Gateway.Callback.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API 設定和服務
            string path = AppDomain.CurrentDomain.BaseDirectory;
            SerilogLogger.Configure(() =>
                new LoggerConfiguration()
                .Enrich.WithProperty("SourceContext", "PaymentCenter Merchant Callback Gateway Api")
                .Enrich.WithProperty("Environment", "test")
                .Enrich.FromLogContext()
                .MinimumLevel.Debug()
                .WriteTo.File($"{path}\\logs\\serilog.txt", restrictedToMinimumLevel: LogEventLevel.Information, outputTemplate: "[{Timestamp:HH:mm:ss} {Level:u3}] {Properties:j} {Message:lj}{NewLine}{Exception}", rollingInterval: RollingInterval.Day)
                .WriteTo.Seq("http://203.60.2.122:5341", restrictedToMinimumLevel: LogEventLevel.Information)
                .CreateLogger()
            );

            // Web API 路由
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
