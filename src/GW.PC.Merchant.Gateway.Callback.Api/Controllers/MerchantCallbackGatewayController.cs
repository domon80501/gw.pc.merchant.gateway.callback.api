﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Callback.Context;
using GW.PC.Web.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace GW.PC.Merchant.Gateway.Callback.Api.Controllers
{
    [RoutePrefix(WebApiControllerRoutePrefixes.MerchantCallbackGateway)]
    public class MerchantCallbackGatewayController : ApiController
    {
        [Route("request")]
        public new async Task<string> Request(MerchantCallbackGatewayRequestContext model)
        {   
            HttpClient client = null;
            HttpResponseMessage response = null;
            var guid = Guid.NewGuid().ToString();

            try
            {
                client = GetClient(model.Data);

                SerilogLogger.Logger.LogInformation($"Request {guid}: {JsonConvert.SerializeObject(model)}");

                response = await client.PostAsJsonAsync(model.Url, model.Content);

                await response.EnsureSuccessStatusCodeAsync();

                var resultContent = Encoding.UTF8.GetString(await response.Content.ReadAsByteArrayAsync());

                SerilogLogger.Logger.LogInformation($"Response {guid}: {resultContent}");

                return resultContent;
            }
            catch (Exception ex)
            {
                var error = $"Error {guid}: {model.Url}. Response code: {response?.StatusCode} Details: {ex.ToString()}";
                SerilogLogger.Logger.LogError(error);

                throw new Exception(error);
            }
            finally
            {
                client?.Dispose();

                var data = model.Data;
                if (data != null && data.ContainsKey(Constants.GatewayRequestDataItems.IgnoreServerCertificateValidation))
                {
                    ServicePointManager.ServerCertificateValidationCallback -= ServerCertificateValidation;
                }
            }
        }

        private HttpClient GetClient(IDictionary<string, object> data)
        {
            var client = new HttpClient();

            if (data != null)
            {
                if (data.ContainsKey(Constants.GatewayRequestDataItems.IgnoreServerCertificateValidation))
                {
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;
                    ServicePointManager.ServerCertificateValidationCallback += ServerCertificateValidation;
                }

                if (data.ContainsKey(Constants.GatewayRequestDataItems.CertificateFilePath) &&
                    data.ContainsKey(Constants.GatewayRequestDataItems.CertificatePassword))
                {
                    var cert = new X509Certificate2(
                        data[Constants.GatewayRequestDataItems.CertificateFilePath].ToString(),
                        data[Constants.GatewayRequestDataItems.CertificatePassword].ToString(),
                        X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.MachineKeySet);
                    var handler = new WebRequestHandler();
                    handler.ClientCertificates.Add(cert);

                    client = new HttpClient(handler);
                }

                if (data.TryGetValue("headers", out object headers))
                {
                    foreach (var item in ((JObject)headers).ToObject<IDictionary<string, string>>())
                    {
                        client.DefaultRequestHeaders.Add(item.Key, item.Value);
                    }
                }
            }

            return client;
        }
        private bool ServerCertificateValidation(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors errors)
        {
            return true;
        }
    }
}
