﻿using System;
using System.Collections.Generic;

namespace GW.PC.Merchant.Gateway.Callback.Context
{
    public class MerchantCallbackGatewayRequestContext
    {
        public string Url { get; set; }        
        public object Content { get; set; }        
        public IDictionary<string, object> Data { get; set; }
    }
}