﻿using GW.PC.Payment.SDK;
using Newtonsoft.Json;

namespace GW.PC.Merchant.Gateway.Callback.Context
{
    public class MerchantCallbackRequestContext
    {
        /// <summary>
        /// 商户号
        /// </summary>
        [JsonProperty("merchantCode")]
        public string MerchantUsername { get; set; }
        /// <summary>
        /// 商户订单号
        /// </summary>
        [JsonProperty("merchantOrderNo")]
        public string MerchantOrderNumber { get; set; }
        /// <summary>
        /// 支付中心订单号
        /// </summary>
        [JsonProperty("systemOrderNo")]
        public string PaymentCenterOrderNumber { get; set; }
        /// <summary>
        /// 支付商订单号
        /// </summary>
        [JsonProperty("paymentOrderNo")]
        public string PaymentOrderNumber { get; set; }
        /// <summary>
        /// 支付方式
        /// </summary>
        [JsonProperty("payType")]
        public string PaymentType { get; set; }
        /// <summary>
        /// 支付类型
        /// </summary>
        [JsonProperty("payMethod")]
        public string PaymentMethod { get; set; }
        /// <summary>
        /// 请求金额
        /// </summary>
        [JsonProperty("reqAmount")]
        public string RequestedAmount { get; set; }
        /// <summary>
        /// 实际金额
        /// </summary>
        [JsonProperty("amount")]
        public string ActualAmount { get; set; }
        /// <summary>
        /// 交易行为状态
        /// </summary>
        [JsonProperty("status")]
        public string Status { get; set; }
        [SignIgnore]
        [JsonProperty("sign")]
        public string Sign { get; set; }
    }
}
