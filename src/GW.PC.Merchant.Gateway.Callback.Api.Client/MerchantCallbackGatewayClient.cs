﻿using GW.PC.Core;
using GW.PC.Merchant.Gateway.Callback.Context;
using GW.PC.Web.Core;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace GW.PC.Merchant.Gateway.Callback.Api.Client
{
    public class MerchantCallbackGatewayClient : WebApiClientBase
    {
        protected static HttpClient client = null;

        public MerchantCallbackGatewayClient()
        {
            client = client ?? new HttpClient
            {
                BaseAddress = new Uri(Constants.AppSettings.Get(
                    AppSettingNames.MerchantCallbackGatewayWebApiAddress))
            };

            RoutePrefix = WebApiControllerRoutePrefixes.MerchantCallbackGateway;
        }

        protected override HttpClient GetHttpClient()
        {
            return client;
        }

        public Task<string> Request(MerchantCallbackGatewayRequestContext context)
        {
            return PostAsJsonAsyncRawResult<MerchantCallbackGatewayRequestContext, string>("request", context);
        }
    }
}
